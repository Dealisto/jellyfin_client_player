import 'node-self';
import { Controller } from "../src/Controller.js";
import { expect } from 'expect';

// Définissez votre fonction de test ici
async function TestControllerAsync() {
    const controller = new Controller();
    await controller.Connection();
    expect(controller.ApiClient._loggedIn).toBe(true);
    // Vérifiez que l'apiClient et l'authentification sont définis
    expect(controller.ApiClient).toBeDefined();
    expect(controller.Auth).toBeDefined();
    expect(controller.ConnectionManage).toBeDefined();
    // Vérifiez que l'apiClient a l'authentification de la connexion
    expect(controller.ApiClient._serverInfo.AccessToken).toBeDefined();
    expect(controller.ApiClient._serverInfo.UserId).toBeDefined();
    const idUser = await controller.GetUserId();
    expect(controller.Auth.User.Id).toBe(idUser);
    const user = await controller.GetAuth();
    expect(controller.User).toBe(user);
    const items = await controller.GetItems();
    if (items !== null) {
        expect(items).toBeDefined();
        expect(controller.MediaAudio).toBeDefined();
        const durationAudio = await controller.GetDurationAudioAsync(items[items.length - 1]);
        expect(durationAudio).toBeDefined();
        //await controller.PlayPlayerAudioAsync(durationAudio);
    }
    process.exit();
}
// Appelez votre fonction de test ici
TestControllerAsync();