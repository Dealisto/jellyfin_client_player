import 'node-self';
import { ConnectionManage } from '../src/ConnectionManage.js';
import { expect } from 'expect';

// Définissez votre fonction de test ici
async function TestConnectionAsync() {
    const config = {
        Connection: {
            serverAddress: 'http://localhost:8096',
            appName: 'MyApp',
            appVersion: '1.0.0',
            deviceName: 'MyDevice',
            deviceId: 'MyDeviceId'
        },
        Auth: {
            name: 'admin',
            password: 'admin'
        }
    };
    const connectionManage = new ConnectionManage(config);
    const result = await connectionManage.Connection(config);
    expect(connectionManage.ApiClient._loggedIn).toBe(true);
    // Vérifiez que l'apiClient et l'authentification sont définis
    expect(result.apiClient).toBeDefined();
    expect(result.auth).toBeDefined();
    // Vérifiez que l'apiClient a l'authentification de la connexion
    expect(connectionManage.ApiClient._serverInfo.AccessToken).toBe(result.auth.AccessToken);
    expect(connectionManage.ApiClient._serverInfo.UserId).toBe(result.auth.User.Id);
    const idUser = await connectionManage.GetUserId();
    expect(connectionManage.Auth.User.Id).toBe(idUser);
    const user = await connectionManage.GetUser();
    expect(connectionManage.User).toBe(user);
    process.exit();
}
// Appelez votre fonction de test ici
TestConnectionAsync();