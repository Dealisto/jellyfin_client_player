/**
 * L'information des audios.
 */
export class MediaAudio {
  /**
   * L'API client
   */
  ApiClient;
  /**
   * L'authentification
   */
  Auth;
  constructor(apiClient, auth) {
    this.ApiClient = apiClient;
    this.Auth = auth;
  }
  /**
   * Retourne la liste audio dans Jellyfin.
   * @returns La liste audio dans Jellyfin.
   */
  async GetItems() {
    const items = await this.ApiClient.getItems(this.Auth.User.Id, {
      IncludeItemTypes: "Audio",
      Recursive: true,
      Fields: "BasicSyncInfo,MediaStreams,Genres,ParentId,Path,ProductionYear,SortName,Studios,Tags",
      SortBy: "SortName",
      SortOrder: "Ascending"
    });
    return items.Items;
  }
  /**
   * Retourne le temps de l'audio.
   * @param {number} audioDurationTicks - Le nombre de Tick.
   * @returns Le temps de l'audio.
   */
  async GetDurationAudio(audioDurationTicks) {
    // La durée est en ticks, vous pouvez la convertir en secondes en la divisant par 10 000 000
    const audioDurationSeconds = audioDurationTicks / 10000000;
    //console.log(audioDurationSeconds);
    return (audioDurationSeconds + 1);

  }
}