import child_process from 'child_process';
/**
 * Le lecteur audio.
 */
export class Player {
    /**
     * Le constructeur.
     */
    constructor() { }
    /**
     * Joue une liste audio tant que la duration n'est pas fini.
     * @param {list<number>} idAudioDuration - L'identifiant de l'audio avec la duration.
     */
    async Plays(idAudioDuration) {
        for (let position = 0; position < idAudioDuration[0].length; position++) {
            let url = `http://localhost:8096/Audio/${idAudioDuration[0][position]}/stream`;
            //console.log(url);
            let ffplay = await child_process.spawn('ffplay', ['-nodisp', url]);
            ffplay.stderr.on('data', (data) => {
                if (parseInt(data) > idAudioDuration[1][this.position]) {
                    console.log(`Fin de la chanson ${idAudioDuration[0][this.position]}`);
                    ffplay.kill();
                }
            });
            ffplay.stderr.on('exit', (code, signal) => {
                console.log(`ffplay process exited with code ${code} and signal ${signal}`);
            });
            ffplay.stderr.on('close', function () {
                console.log('ffplay closed');
            });
            await new Promise(resolve => setTimeout(resolve, idAudioDuration[1][position] * 1000));
        }
    }

}