import { ConnectionManage } from "./ConnectionManage.js";
import Config from '../Config.json' assert { type: 'json' };
import { MediaAudio } from "./MediaAudio.js";
import { Player } from "./Player.js";
/**
 * La logique de l'application.
 */
export class Controller {
    /**
     * L'apiClient.
     */
    ApiClient;
    /**
     * L'authentification.
     */
    Auth;
    /**
     * Le gestionnaire de connexion.
     */
    ConnectionManage;
    /**
     * Le media audio.
     */
    MediaAudio;
    /**
     * Le constructeur.
     */
    constructor() {
        this.ConnectionManage = new ConnectionManage(Config);
    }
    /**
     * La connexion à l'application jellyfin.
     */
    async Connection() {
        const { apiClient, auth } = await this.ConnectionManage.Connection(Config);
        this.ApiClient = apiClient;
        this.Auth = auth;
    }
    /**
     * Retourne l'authentification de l'utilisateur.
     * @returns L'authentification de l'utilisateur.
     */
    async GetAuth() {
        return await this.ConnectionManage.GetUser();
    }
    /**
     * Retourne l'identifiant de l'utilisateur.
     * @returns L'identifiant de l'utilisateur.
     */
    async GetUserId() {
        return await this.ConnectionManage.GetUserId();
    }
    /**
     * Retourne la liste audio enregistré dans l'application Jellyfin.
     * @returns La liste audio enregistré dans l'application Jellyfin.
     */
    async GetItems() {
        this.MediaAudio = new MediaAudio(this.ApiClient, this.Auth);
        return await this.MediaAudio.GetItems();
    }
    /**
     * Retourne une double liste avec l'identifiant et la durée de l'audio.
     * @param {object} audio - L'information de l'audio.
     * @returns Une double liste avec l'identifiant et la durée de l'audio.
     */
    async GetDurationAudioAsync(audio) {
        const idClientDuration = [[], []];
        idClientDuration[0].push(audio.Id);
        idClientDuration[1].push(await this.MediaAudio.GetDurationAudio(audio.RunTimeTicks));
        return idClientDuration;
    }
    /**
     * Set le player et fait jouer la musique avec le bon audio.
     * @param {object} idClientDuration - Détient l'identifiant et la durée d'un audio.
     */
    async PlayPlayerAudioAsync(idClientDuration) {
        const player = new Player();
        await player.Plays(idClientDuration);

    }
}