import 'node-self';
import JellyfinApiClient from "jellyfin-apiclient";

/**
 * La gestion de la classe pour la connexion.
 **/
export class ConnectionManage {
    /**
     * L'api du client de jellyfin.
     **/
    ApiClient;
    /**
     * L'authentification de l'utilisateur.
     */
    Auth;

    /**
     * Le constructeur.
     * @param {json} config - La configuration pour JellyfinApiClient.
     */
    constructor(config) {
        this.ApiClient = new JellyfinApiClient.ApiClient(
            config.Connection.serverAddress,
            config.Connection.appName,
            config.Connection.appVersion,
            config.Connection.deviceName,
            config.Connection.deviceId
        );
    }
    /**
     * Retourne l'apiClient avec l'authentification de la connexion.
     * @param {json} config - La configuration pour JellyfinApiClient.
     * @returns L'apiClient avec l'authentification de la connexion.
     */
    async Connection(config) {
        this.Auth = await this.ApiClient.authenticateUserByName(config.Auth.name, config.Auth.password);
        await this.SetAuth(this.Auth);
        return { apiClient: this.ApiClient, auth: this.Auth };
    }
    /**
     * Retourne l'information de l'utilisateur.
     * @param {string} name - nom de l'utilisateur.
     * @param {string} password - le mot de passe de l'utilisateur.
     * @returns L'information de l'utilisateur.
     */
    async GetUser() {
        return await this.ApiClient.User;
    }
    /**
     * Retourne l'identifiant de l'utilisateur.
     * @returns L'identifiant de l'utilisateur.
     */
    async GetUserId() {
        return this.Auth.User.Id;
    }
    /**
     * Pour "set" l'authentification sur Jellyfin.
     **/
    /**
     * Set les informations dans l'apiClient de l'utilisateur.
     * @param {object} auth 
     */
    async SetAuth(auth) {
        await this.ApiClient.setAuthenticationInfo(auth.AccessToken, auth.User.Id);
    }
}