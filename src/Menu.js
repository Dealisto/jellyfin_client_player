import { Controller } from "./Controller.js"
import Readline from 'readline';
/**
 * L'affichage des menus.
 */
export class Menu {
  /**
   * La liste audio.
   */
  ListAudio;
  /**
   * Le contrôleur.
   */
  Controller;
  /**
   * Le constructeur.
   */
  constructor() {
    this.Controller = new Controller();
  }
  /**
   * Le gestionnaire des sous menus.
   */
  async MainMenu() {
    const rl = Readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    const askChoices = (question) => {
      return new Promise(resolve => {
        rl.question(question, answer => {
          resolve(answer);
        });
      });
    }
    let choice;
    // Menu de connexion
    await this.MenuConnection(askChoices, choice);
    // Menu principal
    await this.MenuPrincipal(askChoices, choice);
    rl.close();
  }
  /**
   * Le menu de connexion affichant le choix de connexion.
   * @param {object} askChoices 
   * @param {number} choice 
   */
  async MenuConnection(askChoices, choice) {
    let isConnected = false;
    do {
      console.log("Menu de connexion :");
      console.log("0. Se connecter");
      console.log("1. Quitter");
      choice = parseInt(await askChoices("Entrez votre choix : "));
      if (choice === 0) {
        isConnected = true;
        await this.Controller.Connection();
        this.ListAudio = await this.Controller.GetItems();
        console.log("Vous êtes maintenant connecté !");
      } else if (choice === 1) {
        console.log("Au revoir !");
        process.exit();
      }
    } while (!isConnected);
  }
  /**
   * Le menu principale affichant les audios.
   * @param {object} askChoices 
   * @param {number} choice 
   */
  async MenuPrincipal(askChoices, choice) {
    let isFinish = false;
    do {
      console.log("Menu principal :");
      for (let i = 0; i < this.ListAudio.length; i++) {
        console.log(`${i}. ${this.ListAudio[i].Name}`);
      }
      console.log(`${this.ListAudio.length}. Quitter`);
      choice = parseInt(await askChoices("Entrez votre choix : "));
      if (choice < 0 || choice > this.ListAudio.length || isNaN(choice)) {
        console.log("Choix invalide, veuillez réessayer.");
        continue;
      }
      if (choice === this.ListAudio.length) {
        console.log("Au revoir !");
        isFinish = true;
        process.exit();
      } else {
        await this.MenuPlayAudioAsync(choice);
      }
    } while (!isFinish);
  }
  /**
   * Le menu pour faire jouer l'audio selectionné.
   * @param {number} choix 
   */
  async MenuPlayAudioAsync(choix) {
    if (this.ListAudio[choix] !== null) {
      console.log(`Vous avez choisi l'audio : ${this.ListAudio[choix].Name}`);
      await this.Controller.PlayPlayerAudioAsync(await this.Controller.GetDurationAudioAsync(this.ListAudio[choix]));
    } else {
      console.log(`Une erreur ses produites avec votre choix : ${choix}`);
    }
  }
}