# README
       
# Procedure
## On Linux
### Install Node.js & Player

Open a terminal and follow the installation

        sudo apt-get install curl

        curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -

        sudo apt-get install nodejs

        sudo apt install ffmpeg

Version

        node -v

### How to start a new Node.js project with npm init
Use your Command Line and navigate to the root folder of your project and enter

    Command : npm init

#### Show
This command will ask you some questions to generate a package.json file in your project route that describes all the dependencies of your project. This file will be updated when adding further dependencies during the development process.

    name: (project-name) project-name
    version: (0.0.0) 0.0.1
    description: The Project Description
    entry point: //leave empty
    test command: //leave empty
    git repository: //the repositories url
    keywords: //leave empty
    author: // your name
    license: N/A

After you've finished the process of initializing your project using the Node Package Manager, node.js created a package.jsonfile in your project's root direcotry similar to this one:

    {
        "name": "project-name",
        "version": "0.0.1",
        "description": "Project Description",
        "main": "index.js",
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
    },
    "repository": {
        "type": "git",
        "url": "the repositories url"
    },
    "author": "your name",
    "license": "N/A"
    }

### Install dependencies
#### In package.json 
Insert dependencies under "main":

    "dependencies": {
        "child_process": "^1.0.2",
        "jellyfin-apiclient": "latest",
        "node-fetch": "^3.3.0",
        "node-self": "^1.0.2"
    }
##### On Linux (Debian/Ubuntu)
On Debian/Ubuntu, the ALSA backend is selected by default, so be sure to have the alsa.h header file in place:

    sudo apt-get install libasound2-dev

##### Finish with

    npm install

#### If cloned
When you recently cloned a project that contains a package.json, you can be sure, that the project an its workflow depends on other libraries and scripts. To load all this dependencies open up your command line and navigate to the root of your project and execute:

    npm install

This command will read all the dependencies that were defined in the package.json file and automatically installs them for you.

### Install eslint
Open terminal

        sudo apt install eslint

Go to the root of project file

        npm install eslint
        npm init eslint

## How to run tests
Go to :

        package.json

and run the script you want with the command :

       npm run "name_of_the_script"
## More support or fun go to : [Discord](https://discord.gg/KTjv9dY3tQ)
### Jellyfin-apiclient
Site Web

    https://www.npmjs.com/package/jellyfin-apiclient?activeTab=readme

    https://github.com/jellyfin-archive/jellyfin-apiclient-javascript

Needed

    https://www.npmjs.com/package/node-self?activeTab=readme


### Specifying dependencies Example:
    https://docs.npmjs.com/specifying-dependencies-and-devdependencies-in-a-package-json-file
